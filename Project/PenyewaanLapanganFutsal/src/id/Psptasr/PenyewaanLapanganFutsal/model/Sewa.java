/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.Psptasr.PenyewaanLapanganFutsal.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author USER
 */
public class Sewa {
   private static final long serialVersionUID = -6756463875294313469L;

    private String namaPenyewa;
    private String tanggalMain;
    private String bulanMain;
    private String tahunMain;
    private String jamMain;
    private int jenisLapangan;
    private LocalDateTime waktuMasuk;
    private int lamaPenyewaan;
    private BigDecimal biaya;
    private boolean keluar = false;

    public Sewa() {

    }

    public Sewa(String namaPenyewa, int jenisLapangan, int lamaPenyewaan,String tanggalMain, String bulanMain, String tahunMain, String jamMain, LocalDateTime waktuMasuk, BigDecimal biaya) {
        this.namaPenyewa = namaPenyewa;
        this.tanggalMain = tanggalMain;
        this.bulanMain = bulanMain;;
        this.tahunMain = tahunMain;
        this.jamMain = jamMain;
        this.jenisLapangan = jenisLapangan;
        this.lamaPenyewaan = lamaPenyewaan;
        this.waktuMasuk = waktuMasuk;
        this.biaya = biaya;
    }

    public String getNamaPenyewa() {
        return namaPenyewa;
    }

    public void setNamaPenyewa(String namaPenyewa) {
        this.namaPenyewa = namaPenyewa;
    }

    public String getTanggalMain() {
        return tanggalMain;
    }

    public void setTanggalMain(String tanggalMain) {
        this.tanggalMain = tanggalMain;
    }

    public String getBulanMain() {
        return bulanMain;
    }

    public void setBulanMain(String bulanMain) {
        this.bulanMain = bulanMain;
    }

    public String getTahunMain() {
        return tahunMain;
    }

    public void setTahunMain(String tahunMain) {
        this.tahunMain = tahunMain;
    }

    public String getJamMain() {
        return jamMain;
    }

    public void setJamMain(String jamMain) {
        this.jamMain = jamMain;
    }

    public int getJenisLapangan() {
        return jenisLapangan;
    }

    public void setJenisLapangan(int jenisLapangan) {
        this.jenisLapangan = jenisLapangan;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public int getLamaPenyewaan() {
        return lamaPenyewaan;
    }

    public void setLamaPenyewaan(int lamaPenyewaan) {
        this.lamaPenyewaan = lamaPenyewaan;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }


    @Override
    public String toString() {
        return "PenyewaanLapanganFutsal{" + "namaPenyewa =" + namaPenyewa + ", tanggalMain =" + tanggalMain + ", bulanMain =" + bulanMain + ", tahunMain =" + tahunMain + ", jamMain =" + jamMain + ", lamaPenyewa =" + lamaPenyewaan + ", jenisLapangan =" + jenisLapangan + ", waktuMasuk=" + waktuMasuk + ", biaya=" + biaya + ", keluar=" + keluar + '}';
    }

   
}
  

