/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.Psptasr.PenyewaanLapanganFutsal.Controller;

import com.google.gson.Gson;
import id.Psptasr.PenyewaanLapanganFutsal.model.Sewa;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author USER
 */
public class SewaController {

    private static final String FILE = "D:\\Gitlab\\11190910000003_inf2007\\Project\\SewaLapangan.json";
    private Sewa sewa;
    private final Scanner in;
    private String namaPenyewa;
    private String tanggalMain;
    private String bulanMain;
    private String tahunMain;
    private String jamMain;
    private int jenisLapagan;
    private int lamaPenyewaan;
    private final LocalDateTime waktuMasuk;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public SewaController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setSewaLapangan() {
        System.out.print("Nama Penyewa = ");
        namaPenyewa = in.next();
        System.out.print("Tanggal Main = ");
        tanggalMain = in.next();
        System.out.print("Bulan Main = ");
        bulanMain = in.next();
        System.out.print("Tahun Main = ");
        tahunMain = in.next();
        System.out.print("Jam Main = ");
        jamMain = in.next();

        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu Penyewaan = " + formatWaktuMasuk);

        System.out.println("Jenis Lapangan 1 = Rumput sintetis, 2 = Lapangan vinyll");
        System.out.print("Pilih Jenis Lapangan = ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" is not a valid number.\n", input);
            System.out.print("Pilih Jenis Lapangan ");

        }
        jenisLapagan = in.nextInt();
        System.out.print("Berapa jam anda ingin sewa ? ");
        lamaPenyewaan = in.nextInt();
        if (lamaPenyewaan > 0) {
            biaya = new BigDecimal(lamaPenyewaan);
            if (jenisLapagan == 1) {
                biaya = biaya.multiply(new BigDecimal(75000));
            } else {
                biaya = biaya.multiply(new BigDecimal(100000));
            }
        }
        sewa = new Sewa();
        sewa.setNamaPenyewa(namaPenyewa);
        sewa.setTanggalMain(tanggalMain);
        sewa.setBulanMain(bulanMain);
        sewa.setTahunMain(tahunMain);
        sewa.setJamMain(jamMain);
        sewa.setWaktuMasuk(waktuMasuk);
        sewa.setJenisLapangan(jenisLapagan);
        sewa.setLamaPenyewaan(lamaPenyewaan);
        sewa.setBiaya(biaya);

        setWriteSewa(FILE, sewa);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setSewaLapangan();
        }
    }

    public List<Sewa> getReadSewa(String file) {
        List<Sewa> s = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Sewa[] ps = gson.fromJson(line, Sewa[].class);
                s.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SewaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SewaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return s;
    }

    public void setWriteSewa(String file, Sewa sewa) {
        Gson gson = new Gson();

        List<Sewa> sewas = getReadSewa(file);
        sewas.remove(sewa);
        sewas.add(sewa);

        String json = gson.toJson(sewas);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SewaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Sewa getSearch(String namaPenyewa) {
        List<Sewa> sewas = getReadSewa(FILE);

        Sewa p = sewas.stream()
                .filter(pp -> namaPenyewa.equalsIgnoreCase(pp.getNamaPenyewa()))
                .findAny()
                .orElse(null);

        return p;
    }

    public void setPembayaran() {
        System.out.print("Nama Penyewa = ");
        namaPenyewa = in.next();

        Sewa p = getSearch(namaPenyewa);
        if (p != null) {

            System.out.println("Tanggal Main : " + p.getTanggalMain());
            System.out.println("Bulan Main : " + p.getBulanMain());
            System.out.println("Tahun Main : " + p.getTahunMain());
            System.out.println("Jam Main : " + p.getJamMain());
            System.out.println("Jenis Lapangan : " + (p.getJenisLapangan() == 1 ? "Rumput sintetis" : "Lapangan vinyll"));
            System.out.println("Waktu Penyewaan : " + p.getWaktuMasuk().format(dateTimeFormat));
            System.out.println("Biaya : " + p.getBiaya());
            
            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    p.setKeluar(true);
                    setWriteSewa(FILE, p);
                    break;
                case 2:
                    setPembayaran();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setPembayaran();
        }
        System.out.println("Apakah mau memproses kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPembayaran();
        }
    }

    public void getDataSewa() {
        List<Sewa> sewa = getReadSewa(FILE);
        Predicate<Sewa> isKeluar = e -> e.isKeluar() == true;
        Predicate<Sewa> isDate = e -> e.getWaktuMasuk().toLocalDate().equals(LocalDate.now());

        List<Sewa> pResults = sewa.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Sewa::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama penyewa \tWaktu Penyewaan \tJenis Lapangan \t\tWaktu Main \t\t\tHarga");
        System.out.println("----------- \t-------------- \t\t------------- \t\t---------------------- \t\t----------- ");
        pResults.forEach((p) -> {
            System.out.println(p.getNamaPenyewa() + "\t\t" + p.getWaktuMasuk().format(dateTimeFormat) + "\t" + (p.getJenisLapangan()== 1 ? "Rumput sintetis" : "Lapangan vinyll")+ "\t\t" + p.getTanggalMain() + "-" + p.getBulanMain() + "-" + p.getTahunMain() + "\t" + p.getJamMain() + "\t\t" + p.getBiaya());
        });
        System.out.println("----------- \t-------------- \t\t------------- \t\t---------------------- \t\t-----------");
        System.out.println("====================================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataSewa();
        }

    }
}
