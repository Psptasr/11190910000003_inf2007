/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.Psptasr.PenyewaanLapanganFutsal.Controller;

import java.util.Scanner;
import id.Psptasr.PenyewaanLapanganFutsal.model.info;
/**
 *
 * @author USER
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        info info = new info();

        System.out.println("====================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("----------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Sewa Lapangan");
        System.out.println("2. Proses Pembayaran");
        System.out.println("3. Laporan Harian");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("====================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        SewaController pc = new SewaController();
        switch (noMenu) {
            case 1:
                pc.setSewaLapangan();
                break;
            case 2:
                pc.setPembayaran();
                break;
            case 3:
                pc.getDataSewa();
                break;
            case 4:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}

