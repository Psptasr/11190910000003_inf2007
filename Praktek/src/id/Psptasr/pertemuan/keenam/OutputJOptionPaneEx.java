package id.Psptasr.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan: " + bilangan, "Hasil Input", JOptionPane.INFORMATION_MESSAGE);
    }
}
