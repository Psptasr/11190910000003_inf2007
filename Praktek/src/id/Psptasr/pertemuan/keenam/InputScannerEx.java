package id.Psptasr.pertemuan.keenam;
import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan bilangan: ");
        bilangan = in.nextInt();
        
        System.out.println("Bilangan: " + bilangan);
    }
        
}
