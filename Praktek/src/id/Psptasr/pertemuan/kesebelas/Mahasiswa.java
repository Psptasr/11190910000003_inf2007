/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.Psptasr.pertemuan.kesebelas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author USER
 */
public class Mahasiswa implements Serializable {
    private static final long serialVersionUID = 1L;
    private String Nama;
    private String Nim;
    private String kodeMK;
    private int SKS;
    private int Nilai;

    public Mahasiswa() {
    }

    public Mahasiswa(String Nama, String Nim, String kodeMK, int SKS, int Nilai) {
        this.Nama = Nama;
        this.Nim = Nim;
        this.kodeMK = kodeMK;
        this.SKS = SKS;
        this.Nilai = Nilai;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getNim() {
        return Nim;
    }

    public void setNim(String Nim) {
        this.Nim = Nim;
    }

    public String getKodeMK() {
        return kodeMK;
    }

    public void setKodeMK(String kodeMK) {
        this.kodeMK = kodeMK;
    }

    public int getSKS() {
        return SKS;
    }

    public void setSKS(int SKS) {
        this.SKS = SKS;
    }

    public int getNilai() {
        return Nilai;
    }

    public void setNilai(int Nilai) {
        this.Nilai = Nilai;
    }

    @Override
    public String toString() {
        return "Mahasiswa{" + "Nama=" + Nama + ", Nim=" + Nim + ", kodeMK=" + kodeMK + ", SKS=" + SKS + ", Nilai=" + Nilai + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.Nama);
        hash = 89 * hash + Objects.hashCode(this.Nim);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mahasiswa other = (Mahasiswa) obj;
        if (!Objects.equals(this.Nama, other.Nama)) {
            return false;
        }
        if (!Objects.equals(this.Nim, other.Nim)) {
            return false;
        }
        return true;
    }

}