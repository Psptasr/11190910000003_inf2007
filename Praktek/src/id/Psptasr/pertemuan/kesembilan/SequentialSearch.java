package id.Psptasr.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class SequentialSearch {

    public boolean getSearchOutBoolean(int[] L, int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke- " + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke- " + i + " isinya " + L[i]);
        }
        if (L[i] == x) {
            return true;
        } else {
            return false;
        }
    }

    public int getSeqSearchOutIndeks(int[] L, int n, int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
        }
        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }

    public boolean getSeqSearchInBoolean(int[] L, int n, int x) {
        int i = 0;
        boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                    System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
                }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
                System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
            }
        }
        return ketemu;
    }
    
    public int getSearchInIndeks(int[] L, int n, int x) {
        int i = 0;
        boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
            System.out.println("Posisi ke- " + i + " isisnya " + L[i]);
            }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }
    public int getSearchSentinel(int L[], int n, int x){
        int i = 0, idx;
        L[n + 1] = x;
        
        while(L[i] != x){
            i = i + 1;
        }
        if (i == n + 1){
            return idx = -1;
        }else {
            return idx = i;
        }
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i, x, n = 6;
        int L [] = new int [7];
        SequentialSearch app = new SequentialSearch();
        System.out.println("masukan nilai yang dicari = ");
        x = in.nextInt();
        L[0] = 13;
        L[1] = 16;
        L[2] = 14;
        L[3] = 21;
        L[4] = 76;
        L[5] = 15;
        n = L.length - 2;
        System.out.println("indeks ke " + app.getSearchSentinel(L, n, x));
        //System.out.println("indeks = " + app.getSearchOutBoolean(L, n, x));
        //System.out.println("data ada di indeks ke = " + app.getSeqSearchOutIndeks(L, n, x));
        //System.out.println("data = " + app.getSeqSearchInBoolean(L, n, x));
        //System.out.println("indeks = " + app.getSearchInIndeks(L, n, x));
        
    }
}


