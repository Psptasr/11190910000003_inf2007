package id.Psptasr.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class BinarySearch {
      private int k;
    public int getBinarySearch(int[] L, int n, int x) {
        int i = 0;
        int j = n;
        int k = 0;
        boolean ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        }
        else
        {
            return -1;
        }
    }
    public static void main(String[] args) {
        BinarySearch app = new BinarySearch();
        int [] L = new int []{13, 14, 15, 16, 21, 76};
        int n = 6;
        int x;
        Scanner in = new Scanner (System.in);
        System.out.println("masukan nilai x = ");
        x = in.nextInt();
        System.out.println("indeks ke " + app.getBinarySearch(L, n, x) );
    }
    }
