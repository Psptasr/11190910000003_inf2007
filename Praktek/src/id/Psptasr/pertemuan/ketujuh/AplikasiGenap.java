package id.Psptasr.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class AplikasiGenap {
    public static void main(String[] args) {
        int bilangan;
        
        Genap genap = new Genap();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan bilangan");
        bilangan = in.nextInt();
        
        if (genap.getGenap(bilangan)) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }
}
