package id.Psptasr.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class ElemenTerkecil {
    public static int getMin(int A[], int n){
        int i, min = 9999;
        for (i = 0; i < n; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }return min;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i, n;
        System.out.println("Jumlah elemen larik ");
        n = in.nextInt();
        int A[] = new int[n];
        ArrayMaksimum Maksimum = new ArrayMaksimum();
        
        System.out.println("Masukan nilai elemen larik ");
        for (i = 0; i < n; i++) {
            A[i] = in.nextInt();
        }
        System.out.println("Elemen terkecil : " + getMin(A, n));
    }
}
