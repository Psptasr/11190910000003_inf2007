package id.Psptasr.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class MencariNilaiX {
    public static int cariX(int angka[], int X){
    int i;
    for(i = 0; i < angka.length; i++){
        if (angka[i] == X) {
            return i + 1;
        }
    }
    return 0;
}
    public static void main(String[] args) {
        int i, X, n;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan banyaknya elemen");
        n = in.nextInt();
        int angka[] = new int[n];
        System.out.println("Masukan elemen : ");
        
        for (i = 0; i < n; i++) {
            angka[i] = in.nextInt();
        }
        System.out.println("Masukan nilai yang dicari");
        X = in.nextInt();
        int hasil = cariX (angka, X);
        if (hasil == 0){
            System.out.println("Indeks 0");
        }else {
            System.out.println("Urutan ke " + cariX(angka, X));
        }
    }
    }
