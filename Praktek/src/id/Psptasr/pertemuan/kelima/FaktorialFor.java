package id.Psptasr.pertemuan.kelima;
import java.util.Scanner;
/**
 *
 * @author Asus
 */
public class FaktorialFor {
    public static void main(String[] args) {
        int n, fak, i;
        
        Scanner in = new Scanner(System.in);
        System.out.println("masukan nilai faktorial");
        n = in.nextInt();
        fak = 1;
        for ( i = 1; i <= n; i++){
            fak = fak * i;
        }
        System.out.println(" hasil dari faktorial " + n + " adalah " + fak);
    }
}
