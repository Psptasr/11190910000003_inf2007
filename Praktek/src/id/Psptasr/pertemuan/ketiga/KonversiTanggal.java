package id.Psptasr.pertemuan.ketiga;
import java.util.Scanner;
/**
 *
 * @author Asus
 */
public class KonversiTanggal {
    public static void main(String[] args) {
     int jarak, kilometer, meter, sentimeter;
        Scanner in = new Scanner(System.in);
        System.out.print("jarak (cm) : ");
        jarak = in.nextInt();

        kilometer = jarak / 100000;
        meter = (jarak % 100000) / 100;
        sentimeter = (jarak % 100000) % 100;

        System.out.println("jarak = " + kilometer + " km, " + meter + " m, " + sentimeter + " cm");
    }
}
