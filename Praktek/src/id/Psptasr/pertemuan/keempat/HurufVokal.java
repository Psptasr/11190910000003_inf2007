package id.Psptasr.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Asus
 */
public class HurufVokal {

    public static void main(String[] args) {
        char Huruf;
        Scanner input = new Scanner(System.in);
        Huruf = input.next().charAt(0);

        if (Huruf == 'a' || Huruf == 'i' || Huruf == 'u' || Huruf == 'e' || Huruf == 'o') {
            System.out.println("huruf vokal");
        }
    }
}
