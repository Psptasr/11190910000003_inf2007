package id.Psptasr.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Asus
 */
public class NilaiBelanjaSetelahDiskon {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int TotalBelanja = in.nextInt();
        int Diskon, SetelahDiskon;
       
        if (TotalBelanja > 120000) {
            Diskon = TotalBelanja * 7 / 100;
            SetelahDiskon = TotalBelanja - Diskon;
            System.out.println("Diskon = " + Diskon + "\n" + "SetelahDiskon = " + SetelahDiskon );
        }else{
            SetelahDiskon = TotalBelanja;
            System.out.println("SetelahDiskon = " + SetelahDiskon);
        }
    }
}
